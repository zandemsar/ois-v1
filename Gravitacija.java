import java.util.*;

class Gravitacija {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        double c = 6.674 * Math.pow(10,-11);
		double m = 5.972 * Math.pow(10,24);
		double r = 6.371 * Math.pow(10,6);
		int v = sc.nextInt();
		
		double stevilo = Math.pow((v+r),2); 
		double pospesek = c*m / stevilo;
		
        System.out.printf("Pospesek je : %.2f m/s^2 na nadmorski visini : %dm %n",pospesek, v);
    }
}